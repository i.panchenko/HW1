

```python
import numpy as np
import test_samples as ts
import scipy
import matplotlib.pyplot as plt
import scipy as scp

%matplotlib inline
```

Set confidence interval


```python
alpha = 0.05
```

Introduce functions for printing and plotting


```python
def in_crit_region(lower, upper, value):
    if upper == None:
        return not lower < value
    if lower == None:
        return not value < upper
    return not lower < value < upper
```


```python
def print_test(lower, upper, test_value, test_name, 
               summary, acceptance_string, index):
    print('TEST ', index+1)
    print('Critical region (-inf, {}) U ({}, +inf)'\
          .format(lower, upper))
    print('{} value = {}'.format(test_name, test_value))
    print('Summary says: ', summary['Correct hypothesis'])
        
    if in_crit_region(lower, upper, test_value): 
        print('Hypothesis rejected')
        if acceptance_string == summary['Correct hypothesis']:
            print('Type I error')
    else:
        print('Hypothesis accepted')
        if acceptance_string != summary['Correct hypothesis']:
            print('Type II error')
    print('\n')
```


```python
def plot_distr_conf(distr, x_from, x_to, y_scale, 
                    lower_bound, upper_bound, value):
    x_axis = np.arange (x_from, x_to, 0.1)
    plt.figure(figsize=(3.5, 3.5))
    plt.plot(x_axis, distr)
    
    if value is not None:
        plt.fill_between([value, value], 0, y_scale, color='g')
        x_from = value - 1 if value < x_from else x_from
        x_to = value + 1 if value > x_to else x_to
    
    if lower_bound is not None:
        plt.fill_between([x_from, lower_bound],
                         -y_scale/50, y_scale/50, 
                         color='r', alpha=0.5)
    if upper_bound is not None:
        plt.fill_between([upper_bound, x_to],
                         -y_scale/50, y_scale/50, 
                         color='r', alpha=0.5)
    
    plt.show()
```

**Notice:**

Plots contains:
    - test distribution plotted as blue line
    - confidence intervals plotted in red
    - value of test statistics



# Proportion Test


**Goal of the test:**

Assess the true proportions of the population

**How to calculate test statistic:**

$Z = \frac{\hat{p_1} - \hat{p_2}}{\sqrt{\hat{p}(1 - \hat{p})(1/n_1 + 1/n_2)}}$


**What are the parameters of test statistic:**

$p_1$ - proportion of succes for sample 1 

$p_2$ - proportion of succes for sample 2

$\hat{p}$ - proportion of success for the combined sample


**Limitations:**

Applicable for binomial distribution



```python
def proportion_of_success(X):
    return np.count_nonzero(X)/X.shape[0]
```


```python
def binomial_Z_test(p1, p2, p_hat, n1, n2):
    return (p1-p2)/(np.sqrt(p_hat*(1-p_hat)*(1/n1 + 1/n2)))
```


```python
for i in range(5):
    X1, X2, summary = ts.proportion_test_sample()

    p1 = proportion_of_success(X1)
    p2 = proportion_of_success(X2)
    p_hat = proportion_of_success(np.concatenate([X1, X2]))

    Z_value = binomial_Z_test(p1, p2, p_hat, 
                              X1.shape[0], X2.shape[0])
    print_test(lower, upper, Z_value, 'Z test',
               summary, 'Proportions are equal', i)

    lower = scipy.stats.norm.ppf(alpha/2)
    upper = scipy.stats.norm.ppf(1-(alpha/2))

    x_from = -6
    x_to = 6
    plot_distr_conf(
        distr = scipy.stats.norm.pdf(np.arange (x_from, x_to, 0.1), 0, 1), 
        x_from = x_from,
        x_to = x_to,
        y_scale = 0.4,
        lower_bound = lower,
        upper_bound = upper,
        value = Z_value)
```

    TEST  1
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 0.1412139165674322
    Summary says:  Proportions are equal
    Hypothesis accepted
    
    



![png](output_12_1.png)


    TEST  2
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 5.179621073996589
    Summary says:  Proportions are not equal
    Hypothesis rejected
    
    



![png](output_12_3.png)


    TEST  3
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 0.514867708636354
    Summary says:  Proportions are equal
    Hypothesis accepted
    
    



![png](output_12_5.png)


    TEST  4
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = -0.6607730848642056
    Summary says:  Proportions are not equal
    Hypothesis accepted
    Type II error
    
    



![png](output_12_7.png)


    TEST  5
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = -9.860676420728893
    Summary says:  Proportions are not equal
    Hypothesis rejected
    
    



![png](output_12_9.png)


# Chi-square Variance Test

**Goal of the test:**

Test if variance of population is equal to some value.

**How to calculate test statistic:**

$T = (N-1)(s/\sigma^2)$


**What are the parameters of test statistic:**

$\sigma$ - population's standard deviation

$s$ - sample standard deviation

$N$ - sample size

**Limitations:**

We need to know population's standard deviation



```python
def T_test(N, sigma_s, sigma_0):
    return (N-1)*(sigma_s/sigma_0)**2
```


```python
for i in range(5):
    X, summary = ts.variance_test_sample()

    sigma_0 = summary['sigma_0']
    sigma_s = np.std(X)
    N = X.shape[0]
    
    T_value = T_test(N, sigma_s, sigma_0)

    print_test(lower, upper, T_value, 'T test', 
               summary, 'Variance is equal to sigma_0', i)
    
    lower = scipy.stats.chi2.ppf(alpha/2, N-1)
    upper = scipy.stats.chi2.ppf(1-(alpha/2), N-1)
    
    x_from = 0
    x_to = 150
    plot_distr_conf(
        distr = scipy.stats.chi2.pdf(np.arange (x_from, x_to, 0.1), N-1), 
        x_from = x_from, 
        x_to = x_to,
        y_scale = 0.05,
        lower_bound = lower,
        upper_bound = upper,
        value = T_value)
```

    TEST  1
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    T test value = 55.33559725588941
    Summary says:  Variance is not equal to sigma_0
    Hypothesis rejected
    
    



![png](output_16_1.png)


    TEST  2
    Critical region (-inf, 37.21159331171507) U (78.56716489032422, +inf)
    T test value = 110.3322706956818
    Summary says:  Variance is equal to sigma_0
    Hypothesis rejected
    Type I error
    
    



![png](output_16_3.png)


    TEST  3
    Critical region (-inf, 54.623358521559034) U (103.1581119013466, +inf)
    T test value = 30.17118449104528
    Summary says:  Variance is equal to sigma_0
    Hypothesis rejected
    Type I error
    
    



![png](output_16_5.png)


    TEST  4
    Critical region (-inf, 25.214518638112516) U (60.560571734843805, +inf)
    T test value = 10.308348379819073
    Summary says:  Variance is not equal to sigma_0
    Hypothesis rejected
    
    



![png](output_16_7.png)


    TEST  5
    Critical region (-inf, 25.214518638112516) U (60.560571734843805, +inf)
    T test value = 4640.123383622608
    Summary says:  Variance is not equal to sigma_0
    Hypothesis rejected
    
    



![png](output_16_9.png)


# F-Test for Equality of Two Variances

**Goal of the test:**

It is used to compare the variances of two quantitative variables.

**How to calculate test statistic:**

$F = \frac{s_1^2}{s_2^2}$

such that $s_1 > s_2$


**What are the parameters of test statistic:**

$s_1$ - first sample variance

$s_2$ - second sample variance

**Limitations:**
* population must be approximately normally distributed
* the samples must be independent events


```python
def F_test(sigma_1, sigma_2):
    return sigma_1**2 / sigma_2**2
```


```python
for i in range(5):
    X1, X2, summary = ts.variance_equality_test_sample()

    sigma_1 = np.std(X1)
    sigma_2 = np.std(X2)

    N1 = X1.shape[0]
    N2 = X2.shape[0]
    
    if sigma_2 > sigma_1:
        tmp = sigma_2
        sigma_2 = sigma_1
        sigma_1 = tmp
    F_value = F_test(sigma_1, sigma_2)

    lower = scipy.stats.f.ppf(alpha/2, N1-1, N2-1)
    upper = scipy.stats.f.ppf(1-(alpha/2), N1-1, N2-1)

    print_test(lower, upper, F_value, 'F test', 
               summary, 'Variances are equal', i)
    
    x_from = 0.000001
    x_to = 3
    plot_distr_conf(
        distr = scipy.stats.f.pdf(np.arange (x_from, x_to, 0.1), N1-1, N2-1), 
        x_from = x_from, 
        x_to = x_to,
        y_scale = 1.4,
        lower_bound = lower,
        upper_bound = upper,
        value = F_value)
```

    TEST  1
    Critical region (-inf, 0.5575519895085644) U (1.8717529027126458, +inf)
    F test value = 3238.2557835734897
    Summary says:  Variances are not equal
    Hypothesis rejected
    
    



![png](output_20_1.png)


    TEST  2
    Critical region (-inf, 0.6020990203827) U (1.7227453447009193, +inf)
    F test value = 1.3381592775010591
    Summary says:  Variances are equal
    Hypothesis accepted
    
    



![png](output_20_3.png)


    TEST  3
    Critical region (-inf, 0.5497245381370673) U (1.7848081336928665, +inf)
    F test value = 1.0114326099696795
    Summary says:  Variances are equal
    Hypothesis accepted
    
    



![png](output_20_5.png)


    TEST  4
    Critical region (-inf, 0.659672435799587) U (1.526556678349348, +inf)
    F test value = 1.0541931759065548
    Summary says:  Variances are not equal
    Hypothesis accepted
    Type II error
    
    



![png](output_20_7.png)


    TEST  5
    Critical region (-inf, 0.5965996323136495) U (1.6723563219154025, +inf)
    F test value = 1.9649875789405602
    Summary says:  Variances are not equal
    Hypothesis rejected
    
    



![png](output_20_9.png)


# Z-test of correlation coefficient

**Goal of the test:**

Test the hypothesis that the sample and population corellation values are equal.

**How to calculate test statistic:**

$Z = \frac{0.5 * ln\frac{1+\rho }{1-\rho} - 0.5 * ln\frac{1+\rho_0 }{1-\rho_0} }{1/\sqrt{n-3}}$

**What are the parameters of test statistic:**

$\rho$ - correlation coefficient observed on a sample

$\rho_0$ - correlation coefficient observed on a population

$n$ - sample size

**Limitations:**

We don't know the population correlation coefficient


```python
def corellation_Z_test(n, rho_s, rho_0):
    def log_prop(v):
        return 0.5 * np.log((1+v)/(1-v))
    
    return (log_prop(rho_s) - log_prop(rho_0))/(1/np.sqrt(n-3))
```


```python
for i in range(5):
    X, summary = ts.correlation_test_sample()

    x, y = X[:, 0], X[:, 1] 

    rho_0 = summary['rho_0']
    rho_s,_ = scipy.stats.pearsonr(x, y)
    n = X.shape[0]

    Z_value = corellation_Z_test(n, rho_s, rho_0)

    lower = scipy.stats.norm.ppf(alpha/2)
    upper = scipy.stats.norm.ppf(1-(alpha/2))
    
    print_test(lower, upper, Z_value, 'Z test', 
               summary, 'Correlations are equal', i)
    
    x_from = -7
    x_to = 7
    plot_distr_conf(
        distr = scipy.stats.norm.pdf(np.arange (x_from, x_to, 0.1), 0, 1), 
        x_from = x_from, 
        x_to = x_to,
        y_scale = 0.5,
        lower_bound = lower,
        upper_bound = upper,
        value = Z_value)
```

    TEST  1
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = -6.0423737328164835
    Summary says:  Correlations are not equal
    Hypothesis rejected
    
    



![png](output_24_1.png)


    TEST  2
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 0.898466015011935
    Summary says:  Correlations are equal
    Hypothesis accepted
    
    



![png](output_24_3.png)


    TEST  3
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 2.50498971160546
    Summary says:  Correlations are not equal
    Hypothesis rejected
    
    



![png](output_24_5.png)


    TEST  4
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = 1.086612002508878
    Summary says:  Correlations are equal
    Hypothesis accepted
    
    



![png](output_24_7.png)


    TEST  5
    Critical region (-inf, -1.9599639845400545) U (1.959963984540054, +inf)
    Z test value = -0.4477040044723916
    Summary says:  Correlations are not equal
    Hypothesis accepted
    Type II error
    
    



![png](output_24_9.png)


# Chi-square test of independence

**Goal of the test:**


**How to calculate test statistic:**

$T = \sum_{i=1}^{r}{\sum_{j=1}^{c}{\frac{(O_{ij} - E_{ij})^2} {E_{ij}}}}$

**What are the parameters of test statistic:**

$O_{ij}$ - Observed value of two nominal variables

$E_{ij}$ - Expected value of two nominal variables

$r$ - number of rows

$s$ - number of columns

**Limitations:**

* independent observations
* For a 2 by 2 table, all expected frequencies > 5.
* For a larger table, no more than 20% of all cells may have an expected frequency < 5 and all expected frequencies > 1.



```python
def E_i_j(i, j, table, N):
    return (sum(table[i]) * sum(table[:, j]))/N

def O_i_j(i, j, table):
    return table[i,j]

def chi_test(table):
    N = sum(sum(table))
    r = range(len(table))
    c = range(len(table[0]))
    
    res = []
    for i in r:
        for j in c:
            num = (O_i_j(i, j, table) - E_i_j(i, j, table, N))**2
            den = E_i_j(i, j, table, N)
            res.append(num/den)
    return sum(res)
```


```python
for i in range(5):
    X, summary = ts.independence_test_sample()

    x, y = X[:, 0], X[:, 1]

    r = len(np.unique(x))
    c = len(np.unique(y))

    table = np.zeros((r, c))

    for x, y in X:
        table[x, y] += 1

    chi_value = chi_test(table)

    upper = scipy.stats.chi2.ppf(1-(alpha), (r-1)*(c-1))
    
    print_test(None, upper, chi_value, 'Chi test', 
               summary, 'Variables are independent', i)
    
    x_from = 0
    x_to = 40
    plot_distr_conf(
        distr = scipy.stats.chi2.pdf(np.arange (x_from, x_to, 0.1), (r-1)*(c-1)), 
        x_from = x_from, 
        x_to = x_to,
        y_scale = 0.14,
        lower_bound = None,
        upper_bound = upper,
        value = chi_value)
```

    TEST  1
    Critical region (-inf, None) U (12.591587243743977, +inf)
    Chi test value = 30.613155148423008
    Summary says:  Variables are not independent
    Hypothesis rejected
    
    



![png](output_28_1.png)


    TEST  2
    Critical region (-inf, None) U (12.591587243743977, +inf)
    Chi test value = 33.841440377804005
    Summary says:  Variables are not independent
    Hypothesis rejected
    
    



![png](output_28_3.png)


    TEST  3
    Critical region (-inf, None) U (12.591587243743977, +inf)
    Chi test value = 1.9151583710407243
    Summary says:  Variables are independent
    Hypothesis accepted
    
    



![png](output_28_5.png)


    TEST  4
    Critical region (-inf, None) U (12.591587243743977, +inf)
    Chi test value = 23.304615384615385
    Summary says:  Variables are not independent
    Hypothesis rejected
    
    



![png](output_28_7.png)


    TEST  5
    Critical region (-inf, None) U (12.591587243743977, +inf)
    Chi test value = 5.174272511295129
    Summary says:  Variables are independent
    Hypothesis accepted
    
    



![png](output_28_9.png)



```python

```
